 
import React, { Component } from "react";
import todosList from "./todos.json";

class App extends Component {
  state = {
    todos: todosList,
  };

  addTodos = (e) => {
      if(e.key === "Enter") {
        const newTodo = {
          "userId": 1,
          "id": Math.floor(Math.random() * 1000 ),
          "title": e.target.value,
          "completed": false
        } 
      const newTodos = [...this.state.todos, newTodo]
      //newTodos.push(newTodo);
      this.setState({
       ...this.state, todos: newTodos, value: "" 

     });
     //e.target.value = "";
    }
  }

   handleChange = (e) => {
     this.setState({
       ...this.state, value: e.target.value
     })
   }

  toggleTodo = (id) =>{
    const toggleItem = this.state.todos.map((todo) => {
      if(todo.id === id) {
        return {...todo, completed: !todo.completed}
      }
      return todo;
    })
    this.setState((state) => {
      return {
        ...state, todos: toggleItem
      }
    }
    )
  }

  completedItems = (e) => {
    const clearItem = this.state.todos.filter((todo) => todo.completed === false)
    this.setState({
      todos:clearItem
    })
  }

  deleteTodo = (e, id) => {
    const removeTodo = this.state.todos.filter((todo) => todo.id !== id)
    this.setState({
      todos: removeTodo
    })
  }

  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input 
            className="new-todo" 
            placeholder="What needs to be done?" 
            onKeyDown={this.addTodos}
            onChange={this.handleChange}
            autoFocus 
            value={this.state.value}
          /> 
        </header>
        <TodoList 
          todos={this.state.todos}
          toggleTodo={this.toggleTodo} 
          deleteTodo={this.deleteTodo}
          completedItems={this.completedItems}
        />
        <footer className="footer">
          <span className="todo-count">
            <strong>0</strong> item(s) left
          </span>
          <button className="clear-completed" onClick={this.completedItems}>Clear completed</button>
        </footer>
      </section>
    );
  }
}

class TodoItem extends Component {
  render() {
    return (
      <li className={this.props.completed ? "completed" : ""}>
        <div className="view">
          <input 
            className="toggle" 
            type="checkbox" 
            checked={this.props.completed} 
            onChange={this.props.toggleTodo}
          /> 
          <label>
          {this.props.title}
          </label>
          <button 
           className="destroy" 
           onClick={this.props.deleteTodo}
          /> 
        </div>
      </li>
    );
  }
}

class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map((todo) => (
            <TodoItem 
              key={todo.id} 
              title={todo.title} 
              id={todo.id}
              toggleTodo={(e) => this.props.toggleTodo(todo.id)} 
              deleteTodo={(e) => this.props.deleteTodo(e, todo.id)} 
              completed={todo.completed}
            />
          ))}
        </ul>
      </section>
    );
  }
}

export default App;
